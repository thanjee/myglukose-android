package tekhinno.com.au.mygulukose.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import tekhinno.com.au.mygulukose.MyGulukoseConstant;
import tekhinno.com.au.mygulukose.R;

public class VerificationCodeActivity extends BaseActivity {

    @InjectView(R.id.tv_label)
    TextView mTextViewLabel;

    @InjectView(R.id.et_verification_code)
    EditText mEditTextVerificationCode;

    @InjectView(R.id.btn_proceed)
    Button mButtonProceed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification_code);
        ButterKnife.inject(this);
        init();
    }

    public void init() {
        setTextViewTypeface(mTextViewLabel);
        setEditTextTypeface(mEditTextVerificationCode);
        setButtonTypeface(mButtonProceed);

        Bundle bundle = getIntent().getExtras();
        if(bundle!=null){
            mTextViewLabel.setText("Please enter the verification code sent to \n"+bundle.getString(MyGulukoseConstant.BUNDLE_PARENT_EMAIL));
        }
    }

    @OnClick(R.id.btn_proceed)
    public void proceed() {

        String verificationCode = mEditTextVerificationCode.getText().toString().trim();

        if (verificationCode == null || TextUtils.isEmpty(verificationCode)) {
            mEditTextVerificationCode.setError(getString(R.string.error_field_required));
            mEditTextVerificationCode.requestFocus();
            return;
        }

        startActivity(new Intent(this, ProceedActivity.class));
    }
}
