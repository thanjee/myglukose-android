package tekhinno.com.au.mygulukose.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Calendar;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import tekhinno.com.au.mygulukose.MyGulukoseConstant;
import tekhinno.com.au.mygulukose.MyGulukoseHelper;
import tekhinno.com.au.mygulukose.R;
import tekhinno.com.au.mygulukose.dialogs.DatePickerFragment;

public class SignUpActivity extends BaseActivity {

    @InjectView(R.id.et_username)
    EditText mEditTextUserName;

    @InjectView(R.id.til_parent_email)
    TextInputLayout mTextInputLayoutParentEmail;

    @InjectView(R.id.et_first_name)
    EditText mEditTextFirstName;

    @InjectView(R.id.et_last_name)
    EditText mEditTextLastName;

    @InjectView(R.id.et_email)
    EditText mEditTextEmail;

    @InjectView(R.id.et_parent_email)
    EditText mEditTextParentEmail;

    @InjectView(R.id.et_dob)
    TextView mEditTextDob;

    @InjectView(R.id.et_password)
    EditText mEditTextPassword;

    @InjectView(R.id.et_retype_password)
    EditText mEditTextRetypePassword;

    @InjectView(R.id.tv_label)
    TextView mTextViewLabel;

    @InjectView(R.id.tv_terms_and_conditions)
    TextView mTextViewTermsAndConditions;

    @InjectView(R.id.btn_sign_up)
    Button mButtonSignUp;

    @InjectView(R.id.cb_tac)
    CheckBox mCheckBoxTermsAndConditions;


    private int mAge;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        ButterKnife.inject(this);
        init();

    }

    public void init(){
        setEditTextTypeface(mEditTextUserName);
        setEditTextTypeface(mEditTextFirstName);
        setEditTextTypeface(mEditTextEmail);
        setEditTextTypeface(mEditTextParentEmail);

        setTextViewTypeface(mEditTextDob);
        setEditTextTypeface(mEditTextPassword);
        setEditTextTypeface(mEditTextRetypePassword);
        setTextViewTypeface(mTextViewLabel);
        setTextViewTypeface(mTextViewTermsAndConditions);
        setButtonTypeface(mButtonSignUp);

    }

    @OnClick(R.id.btn_sign_up)
    public void signUp() {

        String email = mEditTextEmail.getText().toString().trim();
        String userName = mEditTextUserName.getText().toString().trim();

        if (userName == null || TextUtils.isEmpty(userName)) {

            mEditTextUserName.setError(getString(R.string.error_field_required));
            mEditTextUserName.requestFocus();
            return;
        }

        if (email == null || TextUtils.isEmpty(email)) {

            mEditTextEmail.setError(getString(R.string.error_field_required));
            mEditTextEmail.requestFocus();
            return;
        }

        if (!MyGulukoseHelper.isValidEmail(email)) {
            mEditTextEmail.setError(getString(R.string.error_invalid_email));
            mEditTextEmail.requestFocus();
            return;
        }

        if(!mCheckBoxTermsAndConditions.isChecked()){
            MyGulukoseHelper.showToast(this, getString(R.string.error_check_terms_conditions));
            return;
        }

        Bundle bundle = new Bundle();
        if(isVerify(mAge))
        {
            String parentEmail = mEditTextParentEmail.getText().toString().trim();
            if (parentEmail == null || TextUtils.isEmpty(parentEmail)) {

                mEditTextParentEmail.setError(getString(R.string.error_field_required));
                mEditTextParentEmail.requestFocus();
                return;
            }
            if (!MyGulukoseHelper.isValidEmail(parentEmail)) {
                mEditTextParentEmail.setError(getString(R.string.error_invalid_email));
                mEditTextParentEmail.requestFocus();
                return;
            }

            Intent intent = new Intent(new Intent(SignUpActivity.this, VerificationCodeActivity.class));
            bundle.putString(MyGulukoseConstant.BUNDLE_PARENT_EMAIL,parentEmail);
            intent.putExtras(bundle);
            startActivity(intent);
        }else{
            Intent intent = new Intent(new Intent(SignUpActivity.this, ProceedActivity.class));
            startActivity(intent);
        }
        SignUpActivity.this.finish();
    }

    @OnClick(R.id.ib_dob)
    public void dob() {

        DatePickerFragment datePickerFragment = new DatePickerFragment();
        datePickerFragment.setCallbackListener(new DatePickerFragment.onDatePickerListener() {
            @Override
            public void onDataSet(int year, int month, int day) {
                mEditTextDob.setText(day + "/" + month + "/" + year);
                mAge = calculateAge(year,month,day);

                if(isVerify(mAge)){
                    mTextInputLayoutParentEmail.setVisibility(View.VISIBLE);
                }
            }
        });

        DialogFragment datePicker = datePickerFragment;
        datePicker.show(getSupportFragmentManager(), "datePicker");
    }


    private boolean isVerify(int age){
        if(age <18)
            return true;
        else
            return false;
    }

    private int calculateAge(int year,int month,int day){
        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.set(year, month, day);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)){
            age--;
        }

        return age;
    }
}
