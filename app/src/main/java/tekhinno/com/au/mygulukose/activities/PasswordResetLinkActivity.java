package tekhinno.com.au.mygulukose.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import tekhinno.com.au.mygulukose.R;

public class PasswordResetLinkActivity extends BaseActivity {
    @InjectView(R.id.tv_label)
    TextView mTextViewLabel;

    @InjectView(R.id.btn_back_to_login)
    Button mButtonBackToLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_reset_link);
        ButterKnife.inject(this);
        init();
    }

    public void init(){
        setTextViewTypeface(mTextViewLabel);
        setButtonTypeface(mButtonBackToLogin);
    }

    @OnClick(R.id.btn_back_to_login)
    public void login() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }
}
