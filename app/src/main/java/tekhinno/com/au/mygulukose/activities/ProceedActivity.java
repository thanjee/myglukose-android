package tekhinno.com.au.mygulukose.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import tekhinno.com.au.mygulukose.R;

public class ProceedActivity extends BaseActivity {

    @InjectView(R.id.tv_label)
    TextView mTextViewLabel;

    @InjectView(R.id.tv_label_language)
    TextView mTextViewLabelLanguage;

    @InjectView(R.id.btn_start)
    Button mButtonStart;

    @InjectView(R.id.rb_english)
    RadioButton mRadioButtonEnglish;

    @InjectView(R.id.rb_singhala)
    RadioButton mRadioButtonSinghala;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_proceed);
        ButterKnife.inject(this);
        init();
    }

    public void init() {
        setTextViewTypeface(mTextViewLabel);
        setTextViewTypeface(mTextViewLabelLanguage);
        setButtonTypeface(mButtonStart);
        setToggleButtonTypeface(mRadioButtonEnglish);
        setToggleButtonTypeface(mRadioButtonSinghala);

    }

    @OnClick(R.id.btn_start)
    public void start() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }
}
