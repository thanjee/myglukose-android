package tekhinno.com.au.mygulukose.dialogs;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import java.util.Calendar;

/**
 * Created by Admin on 9/8/16.
 */
public class DatePickerFragment extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {

    private onDatePickerListener mListener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        // Do something with the date chosen by the user
        callListener(year, month, day);
    }

    public DialogFragment setCallbackListener(onDatePickerListener listener){
        mListener = listener;
        return null;
    }

    private void callListener(int year, int month, int day){
        if (mListener != null) mListener.onDataSet(year, month, day);
    }

    public interface onDatePickerListener {
        void onDataSet(int year, int month, int day);
    }
}