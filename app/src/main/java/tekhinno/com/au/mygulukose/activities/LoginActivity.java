package tekhinno.com.au.mygulukose.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import tekhinno.com.au.mygulukose.R;

public class LoginActivity extends BaseActivity {

    @InjectView(R.id.et_email)
    EditText mEditTextEmail;

    @InjectView(R.id.et_password)
    EditText mEditTextPassword;

    @InjectView(R.id.btn_login)
    Button mButtonLogIn;

    @InjectView(R.id.tv_forgot_password)
    TextView mTextViewForgotPassword;

    @InjectView(R.id.tv_label)
    TextView mTextViewLabel;

    @InjectView(R.id.tv_sign_up)
    TextView mTextViewSignUp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ButterKnife.inject(this);
        init();
    }


    public void init(){
        setEditTextTypeface(mEditTextEmail);
        setEditTextTypeface(mEditTextPassword);
        setTextViewTypeface(mTextViewForgotPassword);
        setTextViewTypeface(mTextViewLabel);
        setTextViewTypeface(mTextViewSignUp);
        setButtonTypeface(mButtonLogIn);
    }

    @OnClick(R.id.btn_login)
    public void login() {
        Intent intent = new Intent(this, VerificationCodeActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.tv_forgot_password)
    public void forgotPassword() {
        Intent intent = new Intent(this, ForgotPasswordActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.tv_sign_up)
    public void signUp() {
        Intent intent = new Intent(this, SignUpActivity.class);
        startActivity(intent);
    }

}
