package tekhinno.com.au.mygulukose.activities;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.ToggleButton;

import tekhinno.com.au.mygulukose.R;

public class BaseActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
    }

    public void setTextViewTypeface(TextView textView) {
        Typeface oxygenRegular = Typeface.createFromAsset(getAssets(), "fonts/Oxygen-Regular.ttf");
        textView.setTypeface(oxygenRegular);
    }

    public void setEditTextTypeface(EditText editText) {
        Typeface oxygenRegular = Typeface.createFromAsset(getAssets(), "fonts/Oxygen-Regular.ttf");
        editText.setTypeface(oxygenRegular);
    }

    public void setButtonTypeface(Button button) {
        Typeface oxygenRegular = Typeface.createFromAsset(getAssets(), "fonts/Oxygen-Regular.ttf");
        button.setTypeface(oxygenRegular);
    }

    public void setToggleButtonTypeface(RadioButton radioButton) {
        Typeface oxygenRegular = Typeface.createFromAsset(getAssets(), "fonts/Oxygen-Regular.ttf");
        radioButton.setTypeface(oxygenRegular);
    }
}
