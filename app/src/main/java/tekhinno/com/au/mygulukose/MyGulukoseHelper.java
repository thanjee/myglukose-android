package tekhinno.com.au.mygulukose;

import android.content.Context;
import android.text.TextUtils;
import android.widget.Toast;

/**
 * Created by thanjeedth on 27/09/2016.
 */

public class MyGulukoseHelper {

    public final static boolean isValidEmail(String email) {
        if (TextUtils.isEmpty(email)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
        }
    }

    public static void showToast(Context context, String message) {
        if (message != null)
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

}
