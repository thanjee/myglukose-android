package tekhinno.com.au.mygulukose.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import tekhinno.com.au.mygulukose.MyGulukoseHelper;
import tekhinno.com.au.mygulukose.R;

public class ForgotPasswordActivity extends BaseActivity {
    @InjectView(R.id.et_email)
    EditText mEditTextEmail;

    @InjectView(R.id.tv_label)
    TextView mTextViewLabel;

    @InjectView(R.id.btn_send)
    Button mButtonSend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        ButterKnife.inject(this);

        init();
        }

    public void init(){
        setEditTextTypeface(mEditTextEmail);
        setTextViewTypeface(mTextViewLabel);
        setButtonTypeface(mButtonSend);

    }

    @OnClick(R.id.btn_send)
    public void passwordReset() {

        String email = mEditTextEmail.getText().toString().trim();

        if (email == null || TextUtils.isEmpty(email)) {

            mEditTextEmail.setError(getString(R.string.error_field_required));
            mEditTextEmail.requestFocus();
            return;
        }
        if (!MyGulukoseHelper.isValidEmail(email)) {
            mEditTextEmail.setError(getString(R.string.error_invalid_email));
            mEditTextEmail.requestFocus();
            return;
        }

        Intent intent = new Intent(this, PasswordResetLinkActivity.class);
        startActivity(intent);
    }
}
